<?php

use craft\elements\Entry;
use craft\helpers\UrlHelper;

return [
    'endpoints' => [
        'mainImages' => [
//            HeaderHelp::setHeader([
//                'Access-Control-Allow-Origin' => '*'
//            ]),
            'elementType' => 'craft\elements\Asset',
            'criteria' => ['volumeId' => '1'],
            'transformer' => function (\craft\elements\Asset $mainImage) {
//                var_dump($mainImage->getUrl());
                return [
                    'url' => $mainImage->getUrl()
                ];
//                return getFeatured($mainImage);
            },
        ],
        'news' => function () {
            return [
                'elementType' => Entry::class,
                'criteria' => ['section' => 'news'],
                'transformer' => function (Entry $entry) {
                    return [
                        'title' => $entry->title,
                        'url' => $entry->url,
                        'jsonUrl' => UrlHelper::url("news/{$entry->id}.json"),
                        'summary' => $entry->summary,
                    ];
                },
            ];
        },
        'news/<entryId:\d+>.json' => function ($entryId) {
            return [
                'elementType' => Entry::class,
                'criteria' => ['id' => $entryId],
                'one' => true,
                'transformer' => function (Entry $entry) {
                    return [
                        'title' => $entry->title,
                        'url' => $entry->url,
                        'summary' => $entry->summary,
                        'body' => $entry->body,
                    ];
                },
            ];
        },
    ]
];